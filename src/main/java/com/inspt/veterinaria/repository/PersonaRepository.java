/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inspt.veterinaria.repository;

import com.inspt.veterinaria.model.Persona;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author nmansilla
 */
@Repository
public interface PersonaRepository extends JpaRepository<Persona, Long>{
	
	public ArrayList<Persona> findAll();
	
	public Optional<Persona> findById(Long id);
    
}
