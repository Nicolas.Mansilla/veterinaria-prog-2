package com.inspt.veterinaria.controlador;

import com.inspt.veterinaria.model.Persona;
import com.inspt.veterinaria.service.PersonaService;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author nmansilla
 */
@RestController
@RequestMapping("/p")
public class PersonaController {
    
    @Autowired
    PersonaService personaService;
    
    @GetMapping()
    public ArrayList<Persona> obtenerPersonas(){
        return personaService.getAll() ;
    }
    
    @GetMapping("/{id}")
    public Optional<Persona> obtenerPersonaId(@PathVariable Long id) {
    	return personaService.getPersonaId(id);
    }
    
    @PostMapping()
    public void postPersona(@RequestBody Persona persona) {
    	personaService.postPersona(persona);
    }
    
    
}
