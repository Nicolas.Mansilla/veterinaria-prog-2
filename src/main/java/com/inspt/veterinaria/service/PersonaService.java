package com.inspt.veterinaria.service;

import com.inspt.veterinaria.model.Persona;
import com.inspt.veterinaria.repository.PersonaRepository;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author nmansilla
 */
@Service
public class PersonaService {
    
    @Autowired
    PersonaRepository personaRepository;
    
    public ArrayList<Persona> getAll(){
        return (ArrayList<Persona>) personaRepository.findAll();
    }
    
    public Optional<Persona> getPersonaId(Long id) {
    	return personaRepository.findById(id);
    }
    
    public void postPersona(Persona persona) {
    	personaRepository.save(persona);
    }
}
